__author__ = 'tribble42'

from django import forms
from models import URL


class URLShortenerForm(forms.ModelForm):
    """
    Form to create shortened urls
    """
    url = forms.URLField()

    class Meta:
        model = URL
        fields = ('url', )