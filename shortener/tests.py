__author__ = 'tribble42'


from django.test import TestCase
from models import URL, URLUser


class URLTestCase(TestCase):
    """
    Test case to test URL Model
    """
    def test_create(self):
        URL.objects.create_shortened(URL(url="http://google.com"))


class URLUserTestCase(TestCase):
    """
    Test case to test URLUser Model
    """
    def test_create(self):
        obj = URL.objects.create_shortened(URL(url="http://google.com"))
        URLUser.objects.create(url=obj, ip="localhost")


class ViewTestCase(TestCase):
    """
    Test case to test shortener's views
    """
    def test_home(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_form(self):
        response = self.client.post("/", {'form-url': 'http://google.com'})
        self.assertEqual(response.status_code, 200)

    def test_redirect(self):
        url = URL.objects.create_shortened(URL(url="http://google.com"))
        response = self.client.get('/{0}'.format(url.shortened))
        self.assertEqual(response.status_code, 302)

    def test_redirect1(self):
        url = URL.objects.create_shortened(URL(url="http://google.com"))
        response = self.client.get('/alt/{0}'.format(url.shortened))
        self.assertEqual(response.status_code, 302)

    def test_redirect1_fail(self):
        response = self.client.get('/alt/{0}'.format("foo"))
        self.assertEqual(response.status_code, 404)

    def test_redirect_fail(self):
        response = self.client.get('/{0}'.format("foo"))
        self.assertEqual(response.status_code, 404)