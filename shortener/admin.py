__author__ = 'tribble42'

from django.contrib import admin

from models import URL, URLUser

admin.site.register(URL)


class URLUserAdmin(admin.ModelAdmin):
    list_display = ('url', 'ip', 'creation_date')

admin.site.register(URLUser, URLUserAdmin)