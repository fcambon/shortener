from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'shortener.views.home', name='home'),
    url(r'^shortener/admin/', include(admin.site.urls)),
    url(r'^(?P<short>\w+)/details/$', 'shortener.views.details', name='details'),
    url(r'^(?P<short>\w+)/map/$', 'shortener.views.get_user_map', name='user_map'),
    url(r'^(?P<short>\w+)$', 'shortener.views.redirect', name='redirect'),
    url(r'^alt/(?P<short>\w+)$', 'shortener.views.redirect1', name='redirect1'),
)
