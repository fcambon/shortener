__author__ = 'tribble42'

from django.db import models
from django.contrib.gis.utils import GeoIP, GeoIPException
from my_base64 import encode_int


class URLManager(models.Manager):
    """
    Manager for URL model
    Custom create method to generate the shortened url
    """

    def create_shortened(self, url):
        url.save()
        # We have no way of knowing the id before calling save (calculated by db, not django)
        # So we need to assign the shortened url and call save again
        url.shortened = encode_int(url.id)
        url.save()
        return url

    def get_lasts(self):
        return self.filter().order_by('-creation_date')[:10]


class URLUser(models.Model):
    """
    Model to track the users redirected on this shortener
    """
    url = models.ForeignKey('URL', related_name="tracked")
    ip = models.CharField(max_length=16)
    creation_date = models.DateTimeField(auto_now_add=True)

    def _get_geoip_data(self):
        """
        Attempts to retrieve MaxMind GeoIP data based upon the visitor's IP
        """
        if not hasattr(self, '_geoip_data'):
            self._geoip_data = None
            try:
                gip = GeoIP(cache=4)
                self._geoip_data = gip.city(self.ip)
            except GeoIPException:
                return
        return self._geoip_data

    geoip_data = property(_get_geoip_data)

    class Meta:
        app_label = 'shortener'


class URL(models.Model):
    """
    Model to store urls
    """
    url = models.CharField(max_length=1024)
    shortened = models.CharField(max_length=12, db_index=True, blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    objects = URLManager()

    def __unicode__(self):
        return str(self.id)

    class Meta:
        app_label = 'shortener'
