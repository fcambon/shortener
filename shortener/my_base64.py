__author__ = 'tribble42'


import struct
from base64 import urlsafe_b64encode, urlsafe_b64decode


def encode_int(value):
    """
    Parse value into binary data, stripping it of additional 0
    Use urlsafe_b64encode to avoid + and /, strip the result of =
    """
    value = struct.pack("<I", value).rstrip("\0")
    return urlsafe_b64encode(value).rstrip("=")


def decode_int(value):
    """
    Decode using urlsafe_b64decode for the same reasons as previously
    Pad the result with = to fill the last byte (?)
    """
    value = urlsafe_b64decode(value + '=' * (4 - len(value) % 4))
    value += "\0" * (4 - len(value))
    return struct.unpack("<I", value)[0]