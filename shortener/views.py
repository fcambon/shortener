__author__ = 'tribble42'

from django.shortcuts import render, get_object_or_404, redirect as redirect_url
from forms import URLShortenerForm
from django.http import Http404, HttpResponse
from json import JSONEncoder
from models import URL, URLUser
from library import get_client_ip
from my_base64 import decode_int
from django.views.decorators.csrf import csrf_exempt


def home(request):
    """
    Home page for the shortener
    Contains a form to shortern URLs
    """
    context = {}
    if request.method == 'POST':
        form = URLShortenerForm(request.POST)
        if form.is_valid():
            # Encode the object id in base64 and format it with the current host to build
            # the shortened url
            url = URL.objects.create_shortened(form.save(commit=False))
            context['shortened_url'] = "{0}/{1}".format(request.get_host(), url.shortened)

    else:
        # GET : empty form
        form = URLShortenerForm()

    context['form'] = form
    # Last urls created to display a history
    context['last_urls'] = URL.objects.get_lasts()

    return render(request, "home.html", context)


def redirect(request, short):
    """
    Decode 'short' from base64 to int
    Retrieve the long URL based on the id decoded and redirect the user to the real webpage
    """
    try:
        obj = URL.objects.get(pk=decode_int(str(short)))
        # Track the user who asked to be redirected
        URLUser.objects.create(url=obj, ip=get_client_ip(request))
        # Finally redirect our user
        return redirect_url(obj.url)
    except Exception:
        raise Http404


def redirect1(request, short):
    """
    Alternative redirect
    Use the shortened url to find the corresponding real URL model (index on required field)
    """
    obj = get_object_or_404(URL, shortened=short)
    # Track the user who asked to be redirected
    URLUser.objects.create(url=obj, ip=get_client_ip(request))
    # Finally redirect our user
    return redirect_url(obj.url)


def details(request, short):
    """
    Details page for a shortened url
    """
    obj = get_object_or_404(URL, shortened=short)
    return render(request, "details.html", {"redirect": obj})


@csrf_exempt
def get_user_map(request, short):
    """
    Retrieve the users who clicked the url to display their location on a Google Map
    """
    if request.is_ajax():
        obj = get_object_or_404(URL, shortened=short)

        try:
            data = []
            for t in obj.tracked.all():
                data.append({'id': t.id, 'geoip': t.geoip_data})
        except:
            return HttpResponse(content='{}', mimetype='text/javascript')

        response = HttpResponse(content=JSONEncoder().encode({"users": data}),
                                mimetype='text/javascript')
        return response
    raise Http404
